# 4. Les microcontrôleurs

  Un microcontrôleur (en notation abrégée µc, ou uc ou encore MCU en anglais) est un circuit intégré qui rassemble les éléments essentiels d'un ordinateur : processeur, mémoires (mémoire morte et mémoire vive), unités périphériques et interfaces d'entrées-sorties. Les microcontrôleurs se caractérisent par un plus haut degré d'intégration, une plus faible consommation électrique, une vitesse de fonctionnement plus faible (de quelques MHz jusqu'à plus d'un GHz) et un coût réduit par rapport aux microprocesseurs polyvalents utilisés dans les ordinateurs personnels.

  Cette définition vient de la page Wikipédia dédiée aux [micocontrôleurs](https://fr.wikipedia.org/wiki/Microcontrôleur). J'ajouterai à cela que les microcontrôleurs permettent de traiter des données correspondant à des mesures physiques, effectuées par des capteurs, et d'accomplir automatiquement certaines tâches programmées et mises dans la mémoire du microcontrôleur.


## 4.1 La puce RP2040

  Cette puce est le premier processeur pour microcontrôleurs de la marque [Raspberry Pi](https://www.raspberrypi.com/products/rp2040/). Celle-ci a un faible coût et offre d'excellentes performances, notamment grace à une grande mémoire et au système d'entrées et sorties programmables (PIO). Plus d'informations sur cette puce sont consultables sur cette [datasheet.](https://datasheets.raspberrypi.com/rp2040/rp2040-datasheet.pdf)

  ![](images/module4/194285942.png)


## 4.2 Le circuit imprimé YD-RP2040

  Le module YD-RP2040 qui nous est fourni est très similaire au [Raspberry Pi Pico](https://www.raspberrypi.com/products/raspberry-pi-pico/) mais possède en plus une led RGB et un bouton poussoir inclus directement sur la carte. Le shéma des entrées et sorties de ce module est donné par la figure ci-dessous. ![](images/module4/yd-rp2040.png)
  Les autres informations relatives à l'utilisation de ce modules se trouvent sur la [datasheet](https://datasheets.raspberrypi.com/pico/pico-datasheet.pdf) du module Raspberry Pi Pico.


## 4.3 Programmer le microcontrôleur

  Il y a trois façons de programmer le microcontrôleur Raspberry Pi :

  1. En utilisant le langage C++ avec C++ SDK.

  2. Avec Arduino, une version simplifiée du langage C++.

  3. Avec le langage python.

### 4.3.1 Installation de C++ SDK

  Pour l'installation sur MacOS, il faut avoir installé [Homebrew](https://brew.sh/index_fr). Homebrew servira a installer les outils nécessaires avec les commandes suivantes 

    brew install cmake
    brew tap ArmMbed/homebrew-formulae
    brew install arm-none-eabi-gcc 
  

  Il faut ensuite télécharger le SDK et je recommande également de télécharger quelques exemples de codes. Pour cela, il faut 

  1. Naviguer dans le terminal jusqu'à l'emplacement souhaité.

  2. Télécharger le SDK avec les commandes

        git clone https://github.com/raspberrypi/pico-sdk.git --branch master
        cd pico-sdk
        git submodule update --init

  3. Remonter au même niveau que le SDK dans l'arborescence des fichiers avec `cd ..` puis télécharger les examples avec 

        git clone https://github.com/raspberrypi/pico-examples.git --branch master     

### 4.3.2 Installation de Arduino-CLI et intégration dans VS Code

  Puisque je ne souhaite pas installer un nouvel éditeur de texte mais que le langage d'Arduino est bien plus simple pour la plupart des utilisateurs que C++ SDK, je ne vais installer que [Arduino-CLI](https://arduino.github.io/arduino-cli/0.31/), qui comporte les même fonction que l'[Arduino-IDE](https://docs.arduino.cc/software/ide-v2) à l'exception de l'éditeur de texte et de l'interface utilisateur.

  1. Sur Mac, installer Arduino-CLI avec homebrew

        brew update
        brew install arduino-cli

    Récupérer l'emplacement d'installation d'Arduino-CLI. Celui-ci s'affichera lors de l'installation avec Homebrew. Si il est introuvable, il peut s'obtenir avec la commande (uniquement sur les systèmes d'exploitation Unix)

        whereis ardduino-CLI


    Dans mon cas, il s'est installé à l'emplacement `/usr/local/bin/arduino-cli`. Vérifier l'installation en ouvrant un terminal et en exécutant la commande suivante

        /usr/local/bin/arduino-cli version

    Si cette commande renvoie la version d'Arduino-CLI, l’installation s'est bien déroulée, si cette commande renvoie une erreur, un problème s'est produit à l'installation. Notons que si Arduino-CLI à été ajouté au *PATH*, il est inutile de récupérer son emplacement exact et il suffit de lancer la commande

        arduino-cli version

    Il existe d'autres façons de l'installer sur Mac. Celles-ci sont détaillées sur [cette page](https://arduino.github.io/arduino-cli/0.31/installation/). 

  2. Ouvrir VS Code et télécharger l'extension Arduino
  ![](images/module4/arduino_ext.png)

  3. Ouvrir les paramètres de VS Code (roue dentée en bas à gauche) cliquer sur `sttings` et suivre `settings > extension > Arduino configuration`. Modifier les paramètres suivants:
    * **Arduino: Command Path**: arduino-cli
    * **Arduino: Path**: /usr/local/Cellar/arduino-cli/0.31.0/bin/
    * **Arduino: Use Arduino Cli**: coché

    ![](images/module4/VSCode_Arduino_settings.png)

  4. Ouvrir la palette de commandes de VS Code (command+maj+P) et chercher `board manager`

    ![](images/module4/VSCode_command_board_manager.jpg)

  5. Ouvrir le board manager et cliquer sur *Additional URLs*

    ![](images/module4/board_manager1.jpg)

  6. Ajouter l'URL suivante
  ```
  https://github.com/earlephilhower/arduino-pico/releases/download/global/package_rp2040_index.json
  ```
  7. Retourner dans le board manager et vérifier le package *"Raspberry Pi Pico/RP2040"* est bien installée.

  ![](images/module4/rp2040_package.jpg)

### 4.3.3 Installation de Python SDK

  1. S'assurer que les extensions VS Code nécessaires sont bien installées. Pour cela, ouvrir un terminal et entrer les commandes suivantes.

          code --install-extension ms-python.python
          code --install-extension visualstudioexptteam.vscodeintellicode
          code --install-extension ms-python.vscode-pylance
          code --install-extension paulober.pico-w-go

  2. Se rendre sur la [documentation de Raspberry Pi Pico - Micropython](https://www.raspberrypi.com/documentation/microcontrollers/micropython.html#drag-and-drop-micropython) et télécharger le fichier `.uf2` qui correspond à la carte utilisée. Dans mon cas, c'est le fichier `Raspberry Pi Pico W` qui a fonctionné.

  3. Connecter le microcontrôleur à l'ordinateur par usb en maintenant le bouton *Boot* enfoncé pour faire apparaitre celui-ci comme un appareil de stockage.

  4. Glisser le fichier `.uf2` téléchargé au point 2 dans le microcontrôleur.

  5. Pour commencer à coder, ouvrir un fichier `.py` dans VS Code.

  6. Ouvrir la palette de commande *(maj+command+P)* et chercher puis cliquer sur "Pico-W-Go > Configure Project".

## 4.4 Premiers pas - Faire clignoter une led monochrome

  Comme je l'ai déjà mentionné, le microcontrôleur qui nous a été fourni a une led RGB, mais aussi une led monochrome. Le but de cette section sera d'utiliser des codes simples provenant de github pour prendre le microcontrôleur en main en allumant cette led.

  **ATTENTION:** Sur notre microcontrôleur YD-RP2040, la led monochrome correspond au port 25.

### 4.4.1 Avec C++ SDK

  1. Ouvrir le code dans Visual Studio Code, notre éditeur de texte favori. Pour cela il faut avoir téléchargé les exemples du point 4.3.1 et suivre `pico-exemples > blink > blink.c`

  2. Le code est le suivant.


        // Copyright (c) 2020 Raspberry Pi (Trading) Ltd.
        // SPDX-License-Identifier: BSD-3-Clause


        #include "pico/stdlib.h"

        int main() {
        #ifndef PICO_DEFAULT_LED_PIN
        #warning blink example requires a board with a regular LED
        #else
          const uint LED_PIN = PICO_DEFAULT_LED_PIN;
          gpio_init(LED_PIN);
          gpio_set_dir(LED_PIN, GPIO_OUT);
          while (true) {
              gpio_put(LED_PIN, 1);
              sleep_ms(250);
              gpio_put(LED_PIN, 0);
              sleep_ms(250);
          }
        #endif
        }


  3. Ouvrir un terminal et créer un dossier `build` dans le dossier `pico-examples` qui contiendra un fichier exécutable par notre microcontrôleur.

        mkdir build

  4. Si comme conseillé au point 4.3.1, le SDK et les exemples sont au même niveau de l'arborescence des dossiers, il faut définir le *PICO_SDK_PATH* avec les commandes

        cd build
        export PICO_SDK_PATH=../../pico-sdk

  5. Préparer la construction en exécutant

        cmake ..

  6. Lancer la construction.

        cd blink
        make -j2

    L'option `-j2` lancera deux tâches en parallèle, ce qui permettra d'accélérer la procédure puisque la puce RP2040 a deux coeurs. 

    Un fichier exécutable `.uf2` aura été créé dans le dossier `blink`.

  7. Connecter la carte à l'ordinateur en maintenant le bouton "Boot" enfoncé de sorte à pouvoir charger un programme. Glisser le fichier `blink.uf2` dans le microcontrôleur reconnu par l'ordinateur comme un appareil de stockage.


  **Remarque:** en principe, il est possible de configurer VS Code pour pouvoir lancer la construction directement depuis l'éditeur de texte. Néanmoins, le faire depuis un terminal évite une étape de configuration laborieuse et n'est finalement pas tellement moins pratique.


### 4.4.2 Avec Arduino

  1. Lancer VS Code et créer un fichier `.ino`.

  2. Le code pour faire clignoter la led est le suivant 

          /*
          Blink
          Turns an LED on for one second, then off for one second, repeatedly.
          Most Arduinos have an on-board LED you can control. On the UNO, MEGA and ZERO
          it is attached to digital pin 13, on MKR1000 on pin 6. LED_BUILTIN is set to
          the correct LED pin independent of which board is used.
          If you want to know what pin the on-board LED is connected to on your Arduino
          model, check the Technical Specs of your board at:
          https://www.arduino.cc/en/Main/Products
          modified 8 May 2014
          by Scott Fitzgerald
          modified 2 Sep 2016
          by Arturo Guadalupi
          modified 8 Sep 2016
          by Colby Newman
          This example code is in the public domain.
          https://www.arduino.cc/en/Tutorial/BuiltInExamples/Blink
          */
          // the setup function runs once when you press reset or power the board
          void setup() {
          // initialize digital pin LED_BUILTIN as an output.
          pinMode(LED_BUILTIN, OUTPUT);
          }
          // the loop function runs over and over again forever
          void loop() {
          digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
          delay(1000);                       // wait for a second
          digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
          delay(1000);                       // wait for a second
          }

  3. Vérifier que le fichier sélectionné, la carte utilisée et le port de transfert sont corrects. Ces informations se trouve en bas de la fenêtre VS Code et il est possible de cliquer dessus pour les modifier.
    
    ![](images/module4/info.png)

  4. Brancher la carte à l'ordinateur grace au port usb-c en maintenant le bouton "Boot" enfoncé de sorte à pouvoir charger un programme.

  5. Dans VS Code, compiler et transférer le programme grace aux boutons dédiés au haut à droite de la fenêtre.
  
    ![](images/module4/compile_upload.jpg)

### 4.4.3 Avec Python

  1. Dans VS Code, créer un fichier `.py` avec le code suivant 

          from machine import Pin, Timer

          led = Pin(25, Pin.OUT)
          t = Timer()
          def f(timer):
              global led
              led.toggle()

          t.init(freq=2.5, mode=Timer.PERIODIC, callback=f)

  2. Ouvrir la palette de commande *(maj+command+P)* et chercher puis cliquer sur "Pico-W-Go > Configure Project".

  3. Connecter le microcontrôleur à l'ordinateur par usb en s'assurant d'avoir déjà suivi les étapes de la section 4.3.3

  4. Établir le lien avec la carte et lancer le programme python sur le microcontrôleur grace aux boutons en bas de la fenêtre.

    ![](images/module4/connect_run.png)

## 4.5 Allumer la led RGB

  Dans cette section, nous allons allumer la led RGB qui correspond au port 23 de notre carte et lui assigner une couleur. Cet exercice permet de se familiariser avec l'utilisation des librairies. Ici, ce sera la librairie [Adafruit NeoPixel](https://github.com/adafruit/Adafruit_NeoPixel).

  **REMARQUE:** jusqu'ici, je m'étais dit que j'allais essayer de tout coder en C++ puisque c'est ce langage qui permet d'exploiter toute la puissance d'un microcontrôleur notamment en permettant d'optimiser la gestion de la mémoire, ... Très vite, je me suis retrouvé face à des exemples de codes C++ complexes qui m'ont fait comprendre que maîtriser la programmation des microcontrôleurs en C++ demanderait un temps de loin supérieur aux 5 crédits alloués à ce cours. Finalement, j'ai décidé de me rabattre sur le langage Arduino, bien plus accessible aux débutants et assez puissant pour les projets qui pourraient entrer dans le cadre de ce cours.

### 4.5.1 Arduino

  1. Avant tout, commencer par installer la librairie [Adafruit NeoPixel](https://github.com/adafruit/Adafruit_NeoPixel) dans VS Code. Pour cela, ouvrir la palette de commande `(Maj+command+P)` et rechercher *library manager*.

  2. Ouvrir le *library manager* et rechercher *Adafruit neopixel*.

  3. Cliquer sur installer.

    ![](images/module4/RGB_library.png)

  4. Créer un fichier `.ino` dans VS Code. Voici un exemple de [code donné par Adafruit](https://github.com/adafruit/Adafruit_NeoPixel/blob/master/examples/simple/simple.ino)

            // NeoPixel Ring simple sketch (c) 2013 Shae Erisson
            // Released under the GPLv3 license to match the rest of the
            // Adafruit NeoPixel library

            #include <Adafruit_NeoPixel.h>
            #ifdef __AVR__
            #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
            #endif

            // Which pin on the Arduino is connected to the NeoPixels?
            #define PIN        23 // On Trinket or Gemma, suggest changing this to 1

            // How many NeoPixels are attached to the Arduino?
            #define NUMPIXELS 1 // Popular NeoPixel ring size

            // When setting up the NeoPixel library, we tell it how many pixels,
            // and which pin to use to send signals. Note that for older NeoPixel
            // strips you might need to change the third parameter -- see the
            // strandtest example for more information on possible values.
            Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

            #define DELAYVAL 500 // Time (in milliseconds) to pause between pixels

            void setup() {
              // These lines are specifically to support the Adafruit Trinket 5V 16 MHz.
              // Any other board, you can remove this part (but no harm leaving it):
            #if defined(__AVR_ATtiny85__) && (F_CPU == 16000000)
              clock_prescale_set(clock_div_1);
            #endif
              // END of Trinket-specific code.

              pixels.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)
            }

            void loop() {
              pixels.clear(); // Set all pixel colors to 'off'

              // The first NeoPixel in a strand is #0, second is 1, all the way up
              // to the count of pixels minus one.
              for(int i=0; i<NUMPIXELS; i++) { // For each pixel...

                // pixels.Color() takes RGB values, from 0,0,0 up to 255,255,255
                // Here we're using a moderately bright green color:
                pixels.setPixelColor(i, pixels.Color(0, 150, 0));

                pixels.show();   // Send the updated pixel colors to the hardware.

                delay(DELAYVAL); // Pause before next pass through loop
              }
            }

  5. Brancher la carte au pc par le port usb-c, puis compiler et transférer le programme vers la carte comme montré précédemment.

### 4.5.2 Python

  L'exemple suivant est un code python permettant d'allumer la led RGB en vert et de l'éteindre automatiquement après 5 secondes et pour une durée de 3 secondes.

    from machine import Pin
    import neopixel
    from time import sleep

    # Initialise 1 neopixel on pin 23
    np = neopixel.NeoPixel(machine.Pin(23), 1)

    while True: # Repeat indefinitely
        np[0] = (0, 150, 0) # RGB color values between 0 and 255
        np.write() # Don't forget this line to make the LED change color
        sleep(5) # Microprocessors waits 5 second
        np[0] = (0, 0, 0) 
        np.write()
        sleep(3)
    
  Pour lancer ce code, il suffit de suivre les étapes suivantes:

  1. Créer un fichier `.py` dans VS Code et copier le code précédent.

  2. Ouvrir la palette de commande *(Maj+Command+P)* et lancer la commande *"Pico-W-Go > Configure Project"*.

  3. Établir le lien avec la le microcontrôleur et lancer le programme en appuyant sur *Run*. 
      

## 4.6 Le capteur DHT20

  Le DHT20 est un capteur numérique de température et d'humidité relative (\(\phi\) ou RH). Ce capteur est souvent utilisé dans les projets d'automatisation, de surveillance environnementale et de contrôle de la qualité de l'air. Si la température est un concept bien défini et familier, l'humidité relative est quant à elle moins familière. Celle-ci correspond au rapport entre la quantité de vapeur d'eau présente dans l'air et la quantité de vapeur d'eau que l'air pourrait contenir potentiellement à une température donnée. De façon plus rigoureuse, elle peut être définie comme le rapport entre la pression partielle de la vapeur d'eau (\(p\)) dans l'air et la pression de vapeur saturante (\(p_s\)) de l'eau à la même température, généralement exprimé en pourcentage. Mathématiquement, l'humidité relative est définie de la façon suivante

  $$ \phi = 100  \frac{p}{p_s} $$

  Dans les mesures qui suivront, je m'intéresserai plus particulièrement à la température. Les caractéristiques techniques du thermomètre sont les suivantes :

  | Paramètre | Valeur (°C) |
  |-----------|-------------|
  | Précision | 0.01   |
  | Exactitude | \(\pm\)0.5 |
  | Répétabilité | \(\pm\)0.1 |
  | Hystérèse | \(\pm\)0.1 |

  Plus d'informations peuvent être trouvées dans la [documentation complète du capteur DHT20](https://cdn.sparkfun.com/assets/8/a/1/5/0/DHT20.pdf).

### 4.6.1 Connecter le DHT20 à la carte du microcontrôleur

  Le capteur DHT20 a 4 broches à relier à différents port de notre carte YD-RP2040. Ceux-ci sont illustrés dans la figure suivante

  ![](images/module4/DHT20_pins.png)

  * Les broches 1 et 3 correspondent à l'alimentation électrique. La broche 1 étant l'arrivée de courant (la borne +) et le broche 3 étant la masse (la borne -).

  * La broche 2 correspond au Serial Data (SDA). Cette broche est utilisée pour l'entrée et la sortie de données du capteur. 

  * La broche 4 correspond au Serial Clock (SCL). Cette broche est utilisée pour synchroniser la communication entre le microprocesseur et le DHT20.

### 4.6.2 Prise de mesure - Code Arduino

    //    FILE: DHT20.ino
    //  AUTHOR: Rob Tillaart
    // PURPOSE: Demo for DHT20 I2C humidity & temperature sensor


    #include "DHT20.h"

    DHT20 DHT;

    uint8_t count = 0;

    void setup()
    {
      // DHT.begin();    //  ESP32 default pins 21 22
      Wire.setSDA(8);
      Wire.setSCL(9);
      Wire.begin();


      Serial.begin(115200);
      Serial.println(__FILE__);
      Serial.print("DHT20 LIBRARY VERSION: ");
      Serial.println(DHT20_LIB_VERSION);
      Serial.println();

      delay(1000);
    }


    void loop()
    {
      if (millis() - DHT.lastRead() >= 1000)
      {
        // READ DATA
        uint32_t start = micros();
        int status = DHT.read();
        uint32_t stop = micros();

        if ((count % 10) == 0)
        {
          count = 0;
          Serial.println();
          Serial.println("Type\tHumidity (%)\tTemp (°C)\tTime (µs)\tStatus");
        }
        count++;

        Serial.print("DHT20 \t");
        // DISPLAY DATA, sensor has only one decimal.
        Serial.print(DHT.getHumidity(), 1);
        Serial.print("\t\t");
        Serial.print(DHT.getTemperature(), 1);
        Serial.print("\t\t");
        Serial.print(stop - start);
        Serial.print("\t\t");
        switch (status)
        {
          case DHT20_OK:
            Serial.print("OK");
            break;
          case DHT20_ERROR_CHECKSUM:
            Serial.print("Checksum error");
            break;
          case DHT20_ERROR_CONNECT:
            Serial.print("Connect error");
            break;
          case DHT20_MISSING_BYTES:
            Serial.print("Missing bytes");
            break;
          case DHT20_ERROR_BYTES_ALL_ZERO:
            Serial.print("All bytes read zero");
            break;
          case DHT20_ERROR_READ_TIMEOUT:
            Serial.print("Read time out");
            break;
          case DHT20_ERROR_LASTREAD:
            Serial.print("Error read too fast");
            break;
          default:
            Serial.print("Unknown error");
            break;
        }
        Serial.print("\n");
      }
    }


### 4.6.3 Prise de mesure - Code Python

  Dans ce cas, la librairie du DHT20 n'est pas comprise dans les librairies de base de Micropython. Il faudra donc la télécharger et mettre les fichiers dans le microcontrôleur. 
  
  1. La librairie DHT20 est disponible sur [cette page](https://github.com/flrrth/pico-dht20). Le dossier `dht20` est celui qui nous intéresse. 

  2. Pour transférer le dossier `dht20` sur le microcontrôleur, télécharger *Rshell*. Pour cela, dans un terminal, entrer

          python -m pip install rshell

  3. Identifier le port utilisé par le microcontrôleur. Pour cela enter dans un terminal 

          ls /dev/tty*

    Connecter le microcontrôleur à l'ordinateur et entrer de nouveau la commande précédente. Un nouveau port sera présent, ce sera celui du microcontrôleur. Dans mon cas, le port est `/dev/tty.usbmodem14301`.

  4. Lancer rshell avec la commande 

        rshell -p /dev/tty.usbmodem14301
  
  5. Trouver le nom du microcontrôleur connecté avec la commande `boards`. Dans mon cas, le nom de la carte est `/pyboard`.

  6. Naviguer jusqu'à l'emplacement du dossier `dht20` et entrer la commande

          cp -R ./dht20 /pyboard

  7. Vérifier que le fichier est bien sur le microcontrôleur avec la commande

          ls /pyboard

  Le code utilisé pour lire les données du capteur et les afficher dans un terminal est le suivant

          from machine import Pin, I2C
          from utime import sleep

          from dht20 import DHT20

          #Définir les pins du SDA et du SLC
          i2c0_sda = Pin(8)
          i2c0_scl = Pin(9)
          i2c0 = I2C(0, sda=i2c0_sda, scl=i2c0_scl)

          #Définir l'adresse et le moyen de communication du capteur
          dht20 = DHT20(0x38, i2c0)

          #Prendre 10 mesures espacées d'une seconde
          n = 10
          for i in range(n):
              measurements = dht20.measurements
              print(f"Temperature: {measurements['t']} °C, humidity: {measurements['rh']} %RH")
              sleep(1)

  Pour lancer le programme, suivre la procédure suivante: 

  1. Créer un fichier `.py` dans VS Code et copier le code précédent.

  2. Ouvrir la palette de commande *(Maj+Command+P)* et lancer la commande *"Pico-W-Go > Configure Project"*.

  3. Établir le lien avec la le microcontrôleur et lancer le programme en appuyant sur *Run*.

### 4.6.4 Mesure physique

  La mesure faite par un capteur ne correspond pas à la grandeur physique réelle, mais à une estimation de celle-ci. L'erreur de mesure est alors définie comme la différence entre la valeur réelle (qui est a priori inconnue) et la valeur mesurée de la grandeur physique. Il existe deux types différents d'erreur de mesure: 

  1. Les erreurs aléatoires, qui varient à chaque mesure.

  2. Les erreurs systématiques, qui correspondent à un biais constant à chaque mesure. Cette erreur correspond à l'exactitude dans le tableau donné précédemment.

  Dans le cas de plusieurs mesures individuelles, l'erreur de mesure est une variable aléatoire. Nous pouvons alors faire une analyse statistique des mesures. Pour un nombre de mesure suffisamment grand, nous nous attendons à ce que l'erreur de mesure suive une distribution gaussienne. Prenons alors 10000 mesure de la température grâce à notre thermomètre. 

  **REMARQUE:** La durée de récolte des 10000 mesures n'est pas négligeable. Il est donc nécessaire d'éliminer un maximum de source de variation de température dans la pièce ou se trouve le capteur et d'optimiser le code de façon à prendre les mesures le plus rapidement possible. Même si j'ai effectué les mesures avec un programme python, je conseille fortement de le faire avec un langage compilé (tel que C) plutôt qu'un langage interprété et d'optimiser le programme pour prendre les mesures à la fréquence maximale tolérée par le capteur.

  Dans notre prise de mesure, nous allons supposer que: 

  * La température dans a varié d'une façon suffisamment faible (moins d'un centième de degré Celsius) sur la durée de la prise de mesure.

  * Python est suffisamment lent pour que le capteur puisse suivre la cadence des prise de mesures.
  
  * Le fait de prendre des mesures rapidement n’entraîne pas de biais.

  Ces hypothèses sont à vérifier.

  En se basant sur le code python donné dans la section précédente, nous allons lire les données du capteur et les écrire dans un fichier `.csv` pour pouvoir les transférer et les traiter.

  1. Ouvrir un fichier `.py` dans VS Code, le configurer comme projet *Pico-W-Go* et copier le code suivant.

          from machine import Pin, I2C
          from utime import sleep

          from dht20 import DHT20

          i2c0_sda = Pin(8)
          i2c0_scl = Pin(9)
          i2c0 = I2C(0, sda=i2c0_sda, scl=i2c0_scl)

          dht20 = DHT20(0x38, i2c0)

          file = open("data.csv", "w")

          n = 10000
          for i in range(n):
              measurements = dht20.measurements
              if (i % 1000 == 0): print(i)
              file.write(str(measurements['t'])+"\n")
              file.flush()
          file.close()
        
    Exécuter le programme de la façon habituelle.

  2. Récupérer le fichier grâce à *rshell*. Pour cela, ouvrir un terminal, naviguer jusqu'au dossier contenant les scripts python du projet et entrer les commandes

          rshell -p /dev/tty.usbmodem14301
          cp /pyboard/data.csv ./data.csv

  3. Dans VS Code, ouvrir un fichier `.py` et copier le code suivant

          import numpy as np
          import matplotlib.pyplot as plt

          from scipy.optimize import curve_fit



          def gaussian(x, mean, amplitude, standard_deviation):
              return amplitude * np.exp( - (x - mean)**2 / (2*standard_deviation ** 2))



          def plot(data):

              fig, ax = plt.subplots()
              bin_heights, bin_borders, _ = plt.hist(data, bins='auto', label='Mesures')
              bin_centers = bin_borders[:-1] + np.diff(bin_borders)/2
              popt, _ = curve_fit(gaussian, bin_centers, bin_heights, p0=[20.4, 100., 0.05])
              print(popt)
              y_fit = gaussian(bin_centers, *popt)
              #R_squared
              ss_res = np.sum((bin_heights - y_fit) ** 2)
              ss_tot = np.sum((bin_heights - popt[0]) ** 2)
              r2 = 1 - (ss_res / ss_tot)
              print("R2 = ", r2)
              textstr = '\n'.join((
              r'$\mu=%.3f$' % (popt[0], ),
              r'$\sigma=%.3f$' % (popt[2], ),
              r'$R^2=%.3f$' % r2 ))
              #Plot
              ax.set_xlabel("Température ")
              ax.set_ylabel("Nombre de mesures")
              props = dict(boxstyle='square', facecolor='white', alpha=0.25)
              ax.text(0.025, 0.80, textstr, transform=ax.transAxes, fontsize=11,
                  verticalalignment='top', bbox=props)
              plt.plot(bin_centers, y_fit, label='Ajustement Gaussien')
              plt.legend()
              plt.show()


          if __name__ == '__main__':

              data = np.loadtxt('./data.csv', delimiter=";")
              plot(data)

    Ce code permet d'ouvrir le fichier `.csv` contenant les 10000 mesures de température et d'en faire un histogramme, d'ajuster une courbe gaussienne à cet histogramme de sorte à obtenir la moyenne et l'écart-type et de vérifier la qualité de l'ajustement. J'ai obtenu une température de \(20.45 \pm 0.02\) °C. 

    ![](images/module4/temperature_plot.png)


### 4.6.5 Allumer une led avec un signal du capteur

  Le but de cette section est de s'exercer à traiter les données du capteur et d'automatiser des tâches en fonction de celles-ci. Puisque nous avons un thermomètre, nous allons écrire un programme qui permettra de surveiller la temérature dans un frigo. La [température maximale conseillée](https://www.favv-afsca.be/consommateurs/viepratique/conservation/temperatures/default.asp) est de 7°C. Pour une température comprise entre 2°C (le réglage minimal de mon frigo) et 7°C, la led RGB sera allumée en vert. Au dessus de 7°C, la led s'allumera en rouge et sous les 2°C la lumière sera bleue.

  Le code Arduino pour ce programme est le suivant:

    #include <Adafruit_NeoPixel.h>
    #ifdef __AVR__
        #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
    #endif
    #include "DHT20.h"

    #define PIN        23
    #define NUMPIXELS 1

    Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

    DHT20 DHT;

    int8_t count = 0;

    void setup() {

        // These lines are specifically to support the Adafruit Trinket 5V 16 MHz.
        // Any other board, you can remove this part (but no harm leaving it):
        #if defined(__AVR_ATtiny85__) && (F_CPU == 16000000)
        clock_prescale_set(clock_div_1);
        #endif
        // END of Trinket-specific code.

        pixels.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)

        // DHT.begin();    //  ESP32 default pins 21 22
        Wire.setSDA(8);
        Wire.setSCL(9);
        Wire.begin();


        Serial.begin(115200);
        Serial.println(__FILE__);
        Serial.print("DHT20 LIBRARY VERSION: ");
        Serial.println(DHT20_LIB_VERSION);
        Serial.println();

        delay(1000);
    }

    void loop() {

        pixels.clear();
        Serial.println("Type\tTemp (°C)\tTime (µs)\tStatus");

        while (count < 10) {

            if (millis() - DHT.lastRead() >= 5000) {    // Break of 5 sec at least between two measurements
                // READ DATA
                uint32_t start = micros();
                int status = DHT.read();
                uint32_t stop = micros();

                double_t temp = DHT.getTemperature();

                if (temp < 2) {
                    pixels.setPixelColor(0, pixels.Color(0, 0, 150));
                    pixels.show();
                }
                else if (temp > 7){
                    pixels.setPixelColor(0, pixels.Color(150, 0, 0));
                    pixels.show();
                }
                else if (temp>2 && temp<7) {
                    pixels.setPixelColor(0, pixels.Color(0, 150, 0));
                    pixels.show();
                }
                else {
                    pixels.clear();
                }

                Serial.print("DHT20 \t");
                // DISPLAY DATA, sensor has only one decimal.
                // Serial.print(DHT.getHumidity(), 1);
                // Serial.print("\t\t");
                Serial.print(temp, 3);
                Serial.print("\t\t");
                Serial.print(stop - start);
                Serial.print("\t\t");
                switch (status)
                {
                case DHT20_OK:
                    Serial.print("OK");
                    break;
                case DHT20_ERROR_CHECKSUM:
                    Serial.print("Checksum error");
                    break;
                case DHT20_ERROR_CONNECT:
                    Serial.print("Connect error");
                    break;
                case DHT20_MISSING_BYTES:
                    Serial.print("Missing bytes");
                    break;
                case DHT20_ERROR_BYTES_ALL_ZERO:
                    Serial.print("All bytes read zero");
                    break;
                case DHT20_ERROR_READ_TIMEOUT:
                    Serial.print("Read time out");
                    break;
                case DHT20_ERROR_LASTREAD:
                    Serial.print("Error read too fast");
                    break;
                default:
                    Serial.print("Unknown error");
                    break;
                }
                Serial.print("\n");

                count++;
            }
        }
        count = 0;
    }

  Ce code est basé sur ceux du capteur DHT20 et de la led RGB que j'ai donné précédemment.


## 4.7 Capteur laser de distance

  Le capteur laser [VL53L0X](https://www.velleman.eu/products/view/?id=450562) est un capteur de distance basé sur la technologie de mesure de temps de vol (Time-of-Flight, ou ToF en anglais). Il utilise un laser infrarouge à 940 nm pour émettre un faisceau lumineux qui rebondit sur la cible et revient au capteur. Le capteur mesure la quantité de temps que le faisceau prend pour aller de l'émetteur à la cible et revenir, ce qui permet de calculer la distance.

  Le VL53L0X fonctionne avec une tension comprise entre 2.6V et 3.5V et est capable de mesurer des distances allant jusqu'à 2 mètres. Il est également capable de mesurer la distance à des objets en mouvement, ce qui le rend particulièrement utile dans les applications de suivi.

  Le capteur est assez petit (4,4 mm x 2,4 mm x 1,0 mm) et dispose d'une interface I2C pour la communication avec un microcontrôleur. Il consomme très peu d'énergie, ce qui en fait une solution idéale pour les applications alimentées par batterie.

  ![](images/module4/vma337.jpg)

### 4.7.1 Branchement au micocontrôleur 

  La correspondance des ports est directement écite sur la carte électronique qui contient le capteur. Le brachement est donc très simple en se rappellant que la tension à fournir est de 3.3V

  ![](images/module4/branchement_tof.jpg)

### Code Arduino

  Des exemples de code et la librairie utile au fonctionnement du capteur sont disponible sur [ce lien](https://www.velleman.eu/downloads/29/vl53l0x_arduino_master_example.zip). Nous nous basrons sur le code `continuous.ino` que nous adapterons. Le code à importer dans l'arduino est 

      /* This example shows how to use continuous mode to take
      range measurements with the VL53L0X. It is based on
      vl53l0x_ContinuousRanging_Example.c from the VL53L0X API.

      The range readings are in units of mm. */

      #include <Wire.h>
      #include <VL53L0X.h>

      VL53L0X sensor;

      void setup()
      {
        Serial.begin(9600);
        Wire.setSDA(8);
        Wire.setSCL(9);
        Wire.begin();

        sensor.init();
        sensor.setTimeout(500);

        // Start continuous back-to-back mode (take readings as
        // fast as possible).  To use continuous timed mode
        // instead, provide a desired inter-measurement period in
        // ms (e.g. sensor.startContinuous(100)).
        sensor.startContinuous();
      }

      void loop()
      {
        Serial.print(sensor.readRangeContinuousMillimeters());
        if (sensor.timeoutOccurred()) { Serial.print(" TIMEOUT"); }

        Serial.println();
      }

















































































<!-- This week I worked on defining my final project idea and started to getting used to the documentation process.

## title 1

"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

### subtitle 1

"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."


## title 2

"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.

## Table 1

| Qty |  Description    |  Price  |           Link           | Notes  |
|-----|-----------------|---------|--------------------------|--------|
| 1   | Material one    |  22.00 $| http://amazon.com/test   |    Order many    |
| 1   | Material two    |  22.00 $| http://amazon.com/test   |        |
| 1   | Material three  |  22.00 $| http://amazon.com/test   |        |
| 1   | Material five   |  22.00 $| http://amazon.com/test   |        |
| 1   | Material eight  |  22.00 $| http://amazon.com/test   |        |
| 1   | Material twelve |  22.00 $| http://amazon.com/test   |        |
| 1   | Material eleven |  22.00 $| http://amazon.com/test   |        |

## Useful links

- [Docsify](https://docsify.js.org/#/)
- [Markdown](https://en.wikipedia.org/wiki/Markdown)


## Code Example

Use the three backticks to separate code.

```
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
```


## Image gallery

![](../images/fablab-machine-logos.svg)

## Video

### From Vimeo

<iframe src="https://player.vimeo.com/video/10048961" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/10048961">Sound Waves</a> from <a href="https://vimeo.com/radarboy">George Gally (Radarboy)</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

### From Youtube

<iframe width="560" height="315" src="https://www.youtube.com/embed/Q3oItpVa9fs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## 3D Models

<div class="sketchfab-embed-wrapper"><iframe width="640" height="480" src="https://sketchfab.com/models/658c8f8a2f3042c3ad7bdedd83f1c915/embed" frameborder="0" allow="autoplay; fullscreen; vr" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
    <a href="https://sketchfab.com/models/658c8f8a2f3042c3ad7bdedd83f1c915?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Dita&#39;s Gown</a>
    by <a href="https://sketchfab.com/francisbitontistudio?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Francis Bitonti Studio</a>
    on <a href="https://sketchfab.com?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a>
</p>
</div> -->
