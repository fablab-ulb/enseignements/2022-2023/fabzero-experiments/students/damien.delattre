# 2. Conception Assistée par Ordinateur (CAO)

Cette semaine nous avons appris les bases de la concetption assistée par ordinateur ( ou CAD, Computer Aided Design). Plus précisément nous avons découvert les logiciels open-source Inkscape, Openscad et Freecad permettant de faire de la modélisation en 2 et 3 dimensions. 

## 2.1 Inkscape

Inkscape est un logiciel de dessin vectoriel qui permet de créer des illustrations, des graphiques, des logos, des schémas, des cartes et bien d'autres choses. Il dispose d'une interface utilisateur intuitive qui facilite la création de dessins vectoriels à partir de zéro ou l'édition de fichiers. Il offre une variété d'outils de dessin, notamment des formes de base, des courbes de Bézier, des lignes droites, des outils de texte, des outils de remplissage et de contour.

Inkscape dispose également d'une large gamme d'options de sélection, de manipulation et de transformation des objets. En outre, il permet de transfomer une image matricielle (.png, .jpg) en image vectorielle (.svg) et d'exporter les images dans ce format pour une intégration facilitée dans un projet de design graphique.

## 2.2 OpensCAD

OpenSCAD est un logiciel qui permet de créer des modèles 3D. Il se distingue des autres logiciels de CAO par sa méthode de modélisation par la programmation. Les utilisateurs peuvent créer des modèles 3D en écrivant du code, plutôt qu'en utilisant des outils de dessin interactifs. Cette méthode de conception permet aux utilisateurs de créer des modèles très précis et reproductibles, ainsi que d'automatiser les tâches de conception répétitives.

OpenSCAD offre une variété d'outils de programmation, notamment des fonctions de géométrie, de transformation et de modification, ainsi que des fonctions de déplacement et d'intersection d'objets. Les utilisateurs peuvent également créer des modèles paramétriques, ce qui signifie que les dimensions et les formes peuvent être ajustées facilement en modifiant les valeurs de paramètres. Ces fonctions sont résumées dans la figure suivante.
![Alt text](images/openscad_cheatsheet.jpg)

En outre, OpenSCAD dispose d'une fonction de prévisualisation en temps réel qui permet aux utilisateurs de voir comment leur modèle 3D sera rendu avant de l'exporter.

## 2.3 FreeCAD

FreeCAD est lui aussi un logiciel de modélisation en 3D. Il dispose d'une interface utilisateur qui permet de dessiner et de concevoir des modèles en utilisant des fonctions simples de glisser-déposer, des menus déroulants et des barres d'outils. Les utilisateurs peuvent également programmer des scripts Python pour automatiser des tâches et personnaliser l'interface.

FreeCAD est plus complet qu'OpensCAD et permet même de charger des fichiers générés avec OpensCAD pour les modifier.

## 2.4 FlexLinks

La première tâche qui nous est confiée est celle de créer un modèle 3D d'un [FlexLink](https://www.compliantmechanisms.byu.edu/flexlinks). Ces derniers trouvent leur utilisation dans les cadre des "compliant mechanisms" ou pour utiliser le terme français, les méchanismes souples. Ceux-ci sont des mécanismes qui tirent au moins une partie de leur mobilité de la déflexion d'éléments flexibles plutôt que des articulations mobiles uniquement.

Pour ce faire j'utiliserai le logiciel OpensCAD. Afin que le code soit le plus élégant et le plus facilement modifiable possible, je l'écrirai de façon paramétrique. Je définirai donc tous les paramètres en début de code. 
```
//Global parameters.
$fn = 50;
lenght = 20;
width = 5;
height = 5;
n_hole = 3;
hole_size = 2.5;
bond_lenght = 30;
bond_thickness = 0.5;
```
Puis j'écris un module qui implémente la moitié de mon FlexLink. 
```
//Half module.
module flexkit() {
    union() {
        difference() {
            minkowski(10) {
            cube([lenght-width/2, width/2, height/2]);
            translate([width/4, width/4, 0])
                cylinder(h=height/2, d=width/2);
            }
            for (i = [1:n_hole]) {
                translate([i*lenght/(n_hole+1), width/2, -0.5])
                    cylinder(h=height+1, d=hole_size);
            }
        }
        translate([lenght-(0.1*lenght), 0, height/2-bond_thickness/2])
        cube([bond_lenght/2+(0.1*lenght), width, bond_thickness]);
        }
}
```
Le bloc est créé en utilisant l'opérateur de Minkowski entre un parallélépipède rectangle et un cylindre. Cet opérateur réalise une [somme de Minkowski](https://en.wikipedia.org/wiki/Minkowski_addition) de ces deux objets. De façon plus imagée, si l'on imagine que chaque point de la surface du parallélépipède et de celle du cylindre correspondent à un vecteur qui pointe vers la surface à partir du centre de masse de l'objet, l'addition de ces vecteurs un a un pour le parallélépipède et le cylindre sera la somme de Minkowski et la figure résultante ressemblera à un parallélépipède dont les arêtes laissent place à une courbe.

Finalement j'utiliserai un opérateur de symmétrie mirroir pour compléter le demi lien FlexLink.
```
union() {
    flexkit();
    translate([2*(lenght+bond_lenght/2), 0, 0])
    mirror([1,0,0])
        flexkit();
}
```
J'obtiens alors le résultat suivant 
![flex](images/module2/flexlink.jpg)


## 2.5 Licences Creative Commons (CC)

Les licences Creative Commons sont un ensemble de licences standardisées qui permettent aux créateurs de définir les conditions d'utilisation de leur travail. Voici une description des six types de licences Creative Commons :

* CC0 : Cette licence permet aux créateurs de renoncer à tous les droits d'auteur et de placer leur travail dans le domaine public. Cela signifie que les utilisateurs peuvent copier, modifier, distribuer et utiliser le travail pour toute fin, y compris commerciale, sans demander la permission du créateur.
* CC BY : Cette licence permet aux utilisateurs de partager, copier et distribuer le travail sous réserve d'attribuer le crédit au créateur original. Les utilisateurs peuvent également modifier le travail pour créer des oeuvres dérivées, mais ils doivent toujours attribuer le crédit au créateur d'origine.
* CC BY-SA : Cette licence permet aux utilisateurs de partager, copier et distribuer le travail sous réserve d'attribuer le crédit au créateur original et de diffuser toute oeuvre dérivée sous la même licence Creative Commons. Cela signifie que toute oeuvre dérivée créée à partir du travail original doit être distribuée sous la même licence.
* CC BY-ND : Cette licence permet aux utilisateurs de partager, copier et distribuer le travail sous réserve d'attribuer le crédit au créateur original, mais interdit la création d'oeuvres dérivées. Les utilisateurs peuvent donc diffuser le travail original, mais ne peuvent pas le modifier.
* CC BY-NC : Cette licence permet aux utilisateurs de partager, copier et distribuer le travail sous réserve d'attribuer le crédit au créateur original, mais interdit toute utilisation commerciale du travail. Les utilisateurs peuvent donc diffuser le travail original, mais ne peuvent pas l'utiliser à des fins commerciales.
* CC BY-NC-SA : Cette licence permet aux utilisateurs de partager, copier et distribuer le travail sous réserve d'attribuer le crédit au créateur original, de diffuser toute œuvre dérivée sous la même licence Creative Commons et d'interdire toute utilisation commerciale du travail. Les utilisateurs peuvent donc diffuser le travail original et créer des œuvres dérivées, mais ne peuvent pas les utiliser à des fins commerciales.


## 2.6 Adapter la pièce d'un autre étudiant

J'ai choisi d'adapter une partie de la pièce de [Jules Gerard](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/jules.gerard/fabzero-modules/module02/). Mon but est d'adapter une partie de son code pour avoir des cylindre de rayon différents pour voir lequel s’emboîte le mieux dans la pièce que j'ai créé. Mon code modifié est le suivant 

        /*
            FILE   : model_mod.scad
            
            AUTHOR : Damien Delattre <damien.delattre@ulb.be>
            
            DATE   : 2023-02-25
            
            LICENSE : Creative Commons Attribution-ShareAlike 4.0 Unported [CC BY-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).

            ADAPTED FROM : https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/jules.gerard/fabzero-modules/module02/
        //*/

        $fn=100;
        side=7; // unité paramétrique
        r_trous = 2; // rayons trous
        r_sphere = 2; // rayons sphere

        module barre_p(){ // 

            //barre
        rotate([0,0,90])
        translate([0,0,side*0.5])
            cube([side*10,side*0.8,side],center=true);


        //embout -2 
        rotate([0,0,90])
        translate([10*(side-1)/2,0,side])
            cylinder(side,(r_trous)-0.2,(r_sphere)-0.2,center=true);

            //embout -1 
        rotate([0,0,90])
        translate([10*(side-1)/4,0,side])
            cylinder(side,(r_trous)-0.1,(r_sphere)-0.1,center=true);

            //embout 0 
        rotate([0,0,90])
        translate([0,0,side])
            cylinder(side,(r_trous),(r_sphere),center=true);

            //embout +1
        rotate([0,0,90])
        translate([-10*(side-1)/4,0,side])
            cylinder(side,(r_trous)+0.1,(r_sphere)+0.1,center=true);

        //embout +2
        rotate([0,0,90])
        translate([-10*(side-1)/2,0,side])
            cylinder(side,(r_trous)+0.2,(r_sphere)+0.2,center=true);
        }

        barre_p();

En image, cette pièce ressemble à 

![](images/module2/mod_piece.png)


<!-- # 2. Conception Assistée par Ordinateur (CAO)

This week I worked on defining my final project idea and started to getting used to the documentation process.

## title 1

"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

### subtitle 1

"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."


## title 2

"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.

## Table 1

| Qty |  Description    |  Price  |           Link           | Notes  |
|-----|-----------------|---------|--------------------------|--------|
| 1   | Material one    |  22.00 $| http://amazon.com/test   |    Order many    |
| 1   | Material two    |  22.00 $| http://amazon.com/test   |        |
| 1   | Material three  |  22.00 $| http://amazon.com/test   |        |
| 1   | Material five   |  22.00 $| http://amazon.com/test   |        |
| 1   | Material eight  |  22.00 $| http://amazon.com/test   |        |
| 1   | Material twelve |  22.00 $| http://amazon.com/test   |        |
| 1   | Material eleven |  22.00 $| http://amazon.com/test   |        |

## Useful links

- [Docsify](https://docsify.js.org/#/)
- [Markdown](https://en.wikipedia.org/wiki/Markdown)


## Code Example

Use the three backticks to separate code.

```
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
```


## Image gallery

![](../images/fablab-machine-logos.svg)

## Video

### From Vimeo

<iframe src="https://player.vimeo.com/video/10048961" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/10048961">Sound Waves</a> from <a href="https://vimeo.com/radarboy">George Gally (Radarboy)</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

### From Youtube

<iframe width="560" height="315" src="https://www.youtube.com/embed/Q3oItpVa9fs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## 3D Models

<div class="sketchfab-embed-wrapper"><iframe width="640" height="480" src="https://sketchfab.com/models/658c8f8a2f3042c3ad7bdedd83f1c915/embed" frameborder="0" allow="autoplay; fullscreen; vr" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
    <a href="https://sketchfab.com/models/658c8f8a2f3042c3ad7bdedd83f1c915?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Dita&#39;s Gown</a>
    by <a href="https://sketchfab.com/francisbitontistudio?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Francis Bitonti Studio</a>
    on <a href="https://sketchfab.com?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a>
</p>
</div> -->
