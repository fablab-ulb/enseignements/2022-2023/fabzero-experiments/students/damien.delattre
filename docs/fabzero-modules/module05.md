# 5. Dynamique de groupe et projet final

## 1. Introduction aux méthodes de prise de décisions en groupe: Analyse des arbres de problèmes et d'objectifs

### Introduction

L'analyse des arbres de problèmes et d'objectifs ou problem and objective trees analysis (POT/OT) est une approche structurée de résolution de problèmes et de prise de décisions. Cette méthode offre une représentation visuelle d'un problème et de ses objectifs connexes. L'analyse POT aide à identifier les causes profondes d'un problème et à élaborer une feuille de route pour atteindre un ensemble d'objectifs désirés.

### Objectif

La méthode POT est utilisée pour analyser des problèmes complexes et identifier les causes sous-jacentes qui mènent à des résultats indésirables. Cela aide à identifier les relations entre le problème et ses objectifs connexes. L'analyse POT aide également à identifier les différents facteurs et variables qui contribuent au problème.

### Processus

L'analyse POT se compose de deux parties : l'arbre des problèmes et l'arbre des objectifs.

#### Arbre des problèmes :
L'arbre des problèmes est la première composante de la méthode POT. Il est utilisé pour identifier et décrire le problème à résoudre. L'arbre des problèmes aide à comprendre la cause profonde du problème en analysant ses symptômes et ses effets. Pour créer un arbre des problèmes, il faut suivre ces étapes :

* Identifier le problème ou l'enjeu qui doit être abordé.
* Écrire le problème au centre de la page en tant que nœud principal.
* Identifier les symptômes ou effets du problème et les ajouter comme branches au nœud principal.
* Identifier les causes du problème et les ajouter comme racines au noeud principal.
* Continuer à ajouter des branches et des racines jusqu'à atteindre la cause profonde du problème.

#### Arbre des objectifs :
L'arbre des objectifs est la deuxième composante de la méthode POT. Il est utilisé pour élaborer un ensemble d'objectifs désirés pour atteindre la solution. L'arbre des objectifs aide à comprendre la relation entre les différents objectifs et comment ils contribuent à la solution. Pour créer un arbre des objectifs, les étapes sont les suivantes :

* Identifier l' objectif qui doit être atteint pour résoudre le problème.
* Écrire l'objectif au centre de la page en tant que nœud principal.
* Identifier les méthodes qui permettent d'atteindre l'objectif et les ajouter comme racines.
* Identifier les impacts qu'auront nos racines et notre objectif et les ajouter comme branches.
* Continuer à ajouter des racines et des branches jusqu'à atteindre les actions spécifiques nécessaires pour atteindre les objectifs.

### Exemple

Avec mon binôme de soutien [Patrik](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/patrick.dezseri/-/tree/main/docs), nous avons décidé de nous baser sur le projet de [Simone Vitale](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/-/tree/master/docs) réalisé dans le cadre de ce cours "How To Make (almost) Any Experiment Using Digital Fabrication". Son projet intitulé "A second life to the 3D printing filament" consistait à récupérer les déchets plastique des imprimantes 3D du Fablab et à les recycler pour pouvoir les réutiliser dans ces mêmes imprimantes 3D.

#### Arbre des problèmes :

Le problème principal que nous avons identifié est la surproduction de plastique nécessaire pour répondre à sa surconsommation. C'est ce problème qui se trouve au centre de l'arbre. Nous avons ensuite identifié différentes causes à ce problème, par exemple: 

* Le fait que les matières premières pour la production de plastique soient abondantes et que les industries sont déjà encrée dans la production de matières plastique
* Le fait que dans notre société nous préférons jeter et racheter du neuf plutôt que de réutiliser ou réparer.
* Le fait que les capacités de recyclage du plastique sont faibles.
* ...

Ces exemples seront les racines de notre arbre. Les branches quant à elles vont correspondre aux conséquences du problème. Nous avons identifié les conséquences suivantes à la surproduction de plastique : 

* Pollution de (micro)plastiques notamment dans les océans.
* Accumulation de déchets plastiques dans certaines régions du monde.
* Dégradation de la biodiversité. 

![](images/module5/ProblemTree.jpg)


#### Arbre des objectifs :

L'objectif principal que nous avons identifié est la réutilisation ou le recyclage des plastiques. Nous l'avons placé au centre de l'arbre. Nous avons ensuite identifié différentes actions pour atteindre cet objectif, par exemple: 

* Réutiliser le plastique dans les imprimantes 3D.
* Utiliser des plastiques de meilleure qualité, plus durables.
* Nettoyer les zones polluées.
* Utiliser des plastiques non toxiques.

Ces exemples seront les racines de notre arbre. Les branches quant à elles vont correspondre aux impacts qu'aura notre objectif. Nous avons identifié les impacts suivants : 

* Une production de déchets diminuée.
* Une dépollution générale.
* Renforcement de la biodiversité. 

![](images/module5/ObjectiveTree.jpg)


## 2. Formation des groupes de projet

Les étudiants se sont tous retrouvés avec un objet de leur choix, lié à une problématique qui leur tenait a coeur. Dans mon cas, cet objet était un fruit. Celui-ci symbolisait le fait que nous émettons des gaz à effet de serre pour transporter des biens qui viennent parfois de très loin alors que nous avons souvent accès à des biens de meilleure qualité produits à deux pas de chez nous.

Nous nous sommes réunis en cercle pour présenter notre objet, puis nous avons dû nous regrouper entre étudiants dont les objets avaient un lien. Pour notre groupe, le liens entre les objets était la pollution.

Nous avons ensuite du discuter pour établir une thématique plus précise, celle des émission de gaz à effet de serre.

Après nous avons réfléchi à diverse problématiques autour de ce thème. Nous avons identifié les suivantes : 

* Réchauffement climatique.
* Fonte des calottes glacières.
* Acidification des océans.

Nous avons utilisé des méthodes de communication comme le "à ta place, je ..." qui consiste comme le nom le suggère à commencer toutes les phrases de la discussion par "à ta place, je ...".

Si je pouvais donner mon avis personnel sur cette méthode, je dirais qu'elle a pris beaucoup de temps pour un résultat que nous aurions aisément pu atteindre en formant un groupe de projet avec des gens avec qui nous avions déjà fait connaissance lors des premiers cours. 

## 3. Outils de dynamique de groupe

### 3.1 La répartition des roles

  La répartition des rôles lors d'une réunion est un aspect important de la gestion de réunion qui peut aider à assurer une participation efficace et équitable de tous les participants. Voici quelques rôles courants qui peuvent être attribués lors d'une réunion :

  1. L'animateur de la discussion : L'animateur de la discussion est chargé de veiller à ce que les discussions soient constructives et respectueuses. Il encourage les participants à s'écouter les uns les autres et à éviter les débats sans fin ou les discussions hors sujet.
  
  2. Le secrétaire : Le secrétaire est chargé de prendre des notes pendant la réunion. Il doit noter les décisions, les actions à entreprendre, les points à clarifier, etc. Les notes peuvent être partagées avec les participants après la réunion pour rappeler les décisions prises et les actions à entreprendre.
  
  3. Le gestionnaire du temps : Il est chargé de veiller au temps alloué pour chaque point de l'ordre du jour. Il peut donner un avertissement lorsque le temps alloué est écoulé et aider à garder la réunion dans les délais.

  En attribuant des rôles clairs pour chaque participant, il est plus facile de garantir une participation active et équitable à la réunion. Cela aide également à éviter les discussions redondantes, les retours en arrière et les divergences de points de vue. En fin de compte, la répartition des rôles peut contribuer à la réussite de la réunion en permettant d'atteindre les objectifs fixés dans les délais impartis.

### 3.2 La météo d'entrée.

  La technique appelée "météo d'entrée" est une méthode souvent utilisée lors de réunions, pour permettre aux participants d'exprimer leur état d'esprit ou leur niveau d'engagement au début d'une session. Les participants sont invités à partager leur état d'esprit ou leur niveau d'engagement. Par exemple, un participant peut dire "Je me sens comme un ciel nuageux aujourd'hui parce que j'ai beaucoup de préoccupations à l'esprit". Il est important que les participants se sentent à l'aise pour partager leur sentiment sans crainte de jugement ou de critique.

  La météo d'entrée est souvent utilisée pour aider les participants à se connecter entre eux, pour mettre en évidence les préoccupations communes ou les défis à relever, et pour aider les participants à exprimer leur point de vue personnel de manière créative et non menaçante. Cette technique est également considérée comme un moyen de favoriser une culture d'écoute et de respect mutuel au sein du groupe et permet d'éviter que certains comportements ou certaines réactions soient mal interprétées dans le groupe.

### 3.3 Prise de décision - le consensus/le vote

  1. Le consensus : Cette méthode implique que tous les membres du groupe doivent être d'accord sur la décision. Si une seule personne s'oppose à la décision, elle ne peut pas être adoptée. 

  2. Le vote : Cette méthode implique que chaque membre du groupe vote pour une décision. La décision est ensuite prise en fonction du résultat du vote. Cette méthode peut être utilisée lorsque les membres du groupe ont des opinions divergentes et que la prise de décision par consensus est difficile à atteindre.



<!-- This week I worked on defining my final project idea and started to getting used to the documentation process.

## title 1

"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

### subtitle 1

"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."


## title 2

"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.

## Table 1

| Qty |  Description    |  Price  |           Link           | Notes  |
|-----|-----------------|---------|--------------------------|--------|
| 1   | Material one    |  22.00 $| http://amazon.com/test   |    Order many    |
| 1   | Material two    |  22.00 $| http://amazon.com/test   |        |
| 1   | Material three  |  22.00 $| http://amazon.com/test   |        |
| 1   | Material five   |  22.00 $| http://amazon.com/test   |        |
| 1   | Material eight  |  22.00 $| http://amazon.com/test   |        |
| 1   | Material twelve |  22.00 $| http://amazon.com/test   |        |
| 1   | Material eleven |  22.00 $| http://amazon.com/test   |        |

## Useful links

- [Docsify](https://docsify.js.org/#/)
- [Markdown](https://en.wikipedia.org/wiki/Markdown)


## Code Example

Use the three backticks to separate code.

```
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
```


## Image gallery

![](../images/fablab-machine-logos.svg)

## Video

### From Vimeo

<iframe src="https://player.vimeo.com/video/10048961" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/10048961">Sound Waves</a> from <a href="https://vimeo.com/radarboy">George Gally (Radarboy)</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

### From Youtube

<iframe width="560" height="315" src="https://www.youtube.com/embed/Q3oItpVa9fs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## 3D Models

<div class="sketchfab-embed-wrapper"><iframe width="640" height="480" src="https://sketchfab.com/models/658c8f8a2f3042c3ad7bdedd83f1c915/embed" frameborder="0" allow="autoplay; fullscreen; vr" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
    <a href="https://sketchfab.com/models/658c8f8a2f3042c3ad7bdedd83f1c915?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Dita&#39;s Gown</a>
    by <a href="https://sketchfab.com/francisbitontistudio?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Francis Bitonti Studio</a>
    on <a href="https://sketchfab.com?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a>
</p>
</div> -->
