# 3. Impression 3D

Bienvenue dans cette documentation qui vous guidera à travers les étapes nécessaires pour réaliser une impression 3D avec l'imprimante Prusa i3 MK3S/S+.

## Étape 1 : Exporter le modèle 3D

Avant de pouvoir imprimer un objet en 3D, if faut disposer d'un modèle 3D approprié. Reprenons le modèle 3D de la pièce FlexLink réalisé dans le module 2 avec OpensCAD, ou vous pouvez télécharger un modèle 3D préexistante à partir de sites web tels que Thingiverse.

Pour utiliser ce modèle 3D, il faut l'exporter au format STL.

## Étape 2 : Configuration du logiciel d'impression

La Prusa i3 MK3S/S+ utilise le logiciel d'impression open-source PrusaSlicer pour préparer les modèles 3D pour l'impression. Le logiciel  est téléchargeable sur le site web de [Prusa3D](https://www.prusa3d.com/fr/page/prusaslicer_424/).

Une fois le logiciel installé, vous devez configurer les paramètres d'impression pour l'imprimante et le matériau que vous utilisez. Le configurateur se lance automatiquement lors du premier démarrage de PursaSlicer. Il suffit de sélectionner les imprimantes Prusa i3 MK3S et MK3S+ avec une buse de 0.4 mm qui seront celles disponibles au Fablab.
![Alt text](images/module3/PursaSlicer_Config.png)

Il est également possible d'ajuster les paramètres d'impression tels que la hauteur de couche qui donnera un compromis entre durée d'impression qualité de finition et solidité de la pièce, la densité de remplissage qui influera sur la solidité et la durée de l'impression et la température d'impression en fonction de vos besoins.

## Étape 3 : Le G-code

Pour pouvoir imprimer le modèle, il faut que PursaSlicer transforme le fichier STL du modèle 3D en un fichier lisible par l'imprimante, contenant le chemin qui sera parcouru par la buse. Ce fichier est un `.gcode`.

PursaSlicer permet d'importer facilement plusieurs modèle 3D au format STL et de les disposer sur la surface d'impression. Le logiciel permet d'orienter la pièce de sorte à ce que la surface de contact entre la pièce et le plateau d'impression soit maximale. Ceci permet de réduire la probabilité que la pièce bouge pendant l'impression, ce qui pourrait endommager l'imprimante.

Lorsque l'on importe plusieurs fichiers STL, PursaSlicer permet de les disposer automatiquement sur le plateau de façon à réduire les mouvements de la buse.

![](images/module3/PursaSlicer.png)

Avec [Patrik](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/patrick.dezseri/-/tree/main/docs) et [Cosmin](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/cosmin.tudor/-/tree/main/docs), nous avons réunis nos 3 modèles pour générer un seul G-code. Nous avons utilisé le fonction de placement automatique sur le plateau et nous avons paramétré une épaisseur de couche de 0,2 mm et un taux de remplissage de 15%. Avec ces paramètres et nos 3 pièces le logiciel prévoyait un peu plus d'une heure d'impression.

Une fois la disposition et les paramètres définis, il suffit de cliquer sur `découper maintenant`, et si le logiciel ne signale pas de problème, sur `exporter le g-code`.

Si le modèle 3D contient un élément qui doit être imprimé avec un angle de surplomb inférieur à 45°, il nécessite un support pour le soutenir.
Les supports allongent le temps d'impression et sont parfois difficiles à retirer. Il faut donc évaluer s'ils sont vraiment nécessaires.
Dans le PursaSlicer, des supports peuvent être ajoutés automatiquement. Il existe 4 options pour les supports :

* Aucun (par défaut) : à utiliser uniquement pour des pièces sans surplomb et avec de ponts de petite taille
* Supports sur le plateau uniquement : permet d'éviter d'avoir des supports qui commencent dans la pièce (ils sont plus difficiles à retirer)
* Seulement pour les générateurs de supports : avec cette option, il est possible choisir où mettre des supports en ajoutant une forme à l'endroit voulu
* Partout : ajoute des supports partout où c'est nécessaire

Dans l'onglet Réglages d'impression, sur la page Supports il est possible de modifier les paramètres qui définissent où et comment les supports doivent être générés.
Pour un résultat plus net, il est parfois judicieux de jouer sur la distance de contact, l’espacement du motif d’interface, le nombre de couches d’interface, ...



## Étape 4 : Préparation de l'imprimante

Avant d'imprimer, il faut s'assurer assurer que l'imprimante Prusa i3 MK3S est correctement préparée.

Tout d'abord, nous vérifions que le filament est bien chargé dans l'extrudeur et qu'il n'y ait pas de noeud dans la bobine. Il faut également vérifier que l'épaisseur de couche est bien calibrée.

Ensuite, il faut nettoyer la plaque d'impression avec de l'alcool ou un dissolvant pour éliminer toute saleté ou résidu graisseux. Cette étape vise a réduire les possibilité que des impuretés réduisent l'adhérence de la pièce au plateau. 

## Étape 5 : Lancement de l'impression

Pour procéder à l'impression, il faut mettre le fichier `.gcode` sur un support qui sera lisible par l'imprimante. Contre toute attente, il n'y a pas de port usb dans l'imprimante. Nous utiliserons une carte SD pour transférer notre modèle à l'imprimante.

Une fois la carte SD insérée, il suffit de naviguer dans le menu de l'imprimante jusqu'à trouver le fichier a imprimer et de cliquer sur "Imprimer".
L'imprimante commencera alors à chauffer la buse et le plateau d'impression, puis commencera à imprimer le modèle 3D couche par couche.

![](images/module3/840DF8FD-912A-46D1-BF02-FD904D1EEB34_1_105_c.jpeg)



## Étape 6 : Finition de l'impression

Une fois l'impression terminée, nous pouvons retirer l'objet de la plaque d'impression en le décollant doucement. Si des supports ont été nécessaires pour l'impression, nous pouvons également les éliminer en utilisant un outil de coupe approprié.

Enfin, pour améliorer la qualité de finition, nous pouvons nettoyer l'objet avec de l'eau savonneuse ou un nettoyant spécifique pour le matériau utilisé.

## Conclusion

Cette première impression m'a permis de réaliser que le modèle de ma pièce n'était pas du tout adapté à l'utilisation d'un FlexLink. J'aurais du prévoir les trous d'un diamètre plus grand, réduire la longueur ainsi que la largeur des blocs, allonger la partie flexible et diminuer son épaisseur pour augmenter la flexibilité.



<!-- This week I worked on defining my final project idea and started to getting used to the documentation process.

## title 1

"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

### subtitle 1

"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."


## title 2

"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.

## Table 1

| Qty |  Description    |  Price  |           Link           | Notes  |
|-----|-----------------|---------|--------------------------|--------|
| 1   | Material one    |  22.00 $| http://amazon.com/test   |    Order many    |
| 1   | Material two    |  22.00 $| http://amazon.com/test   |        |
| 1   | Material three  |  22.00 $| http://amazon.com/test   |        |
| 1   | Material five   |  22.00 $| http://amazon.com/test   |        |
| 1   | Material eight  |  22.00 $| http://amazon.com/test   |        |
| 1   | Material twelve |  22.00 $| http://amazon.com/test   |        |
| 1   | Material eleven |  22.00 $| http://amazon.com/test   |        |

## Useful links

- [Docsify](https://docsify.js.org/#/)
- [Markdown](https://en.wikipedia.org/wiki/Markdown)


## Code Example

Use the three backticks to separate code.

```
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
```


## Image gallery

![](../images/fablab-machine-logos.svg)

## Video

### From Vimeo

<iframe src="https://player.vimeo.com/video/10048961" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/10048961">Sound Waves</a> from <a href="https://vimeo.com/radarboy">George Gally (Radarboy)</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

### From Youtube

<iframe width="560" height="315" src="https://www.youtube.com/embed/Q3oItpVa9fs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## 3D Models

<div class="sketchfab-embed-wrapper"><iframe width="640" height="480" src="https://sketchfab.com/models/658c8f8a2f3042c3ad7bdedd83f1c915/embed" frameborder="0" allow="autoplay; fullscreen; vr" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
    <a href="https://sketchfab.com/models/658c8f8a2f3042c3ad7bdedd83f1c915?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Dita&#39;s Gown</a>
    by <a href="https://sketchfab.com/francisbitontistudio?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Francis Bitonti Studio</a>
    on <a href="https://sketchfab.com?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a>
</p>
</div> -->
