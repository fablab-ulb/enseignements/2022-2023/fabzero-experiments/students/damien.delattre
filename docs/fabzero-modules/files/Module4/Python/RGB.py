from machine import Pin
import neopixel
from time import sleep

# Initialise 1 neopixel on pin 23
np = neopixel.NeoPixel(machine.Pin(23), 1)

while True: # Repeat indefinitely
    np[0] = (255, 0, 0) # Indicate that first neopixel (=0 in python) should be fully red
    np.write() # Don't forget this line to make the LED change color
    sleep(1) # Microprocessors waits 1 second
    np[0] = (255, 30, 0) # Color orange
    np.write()
    sleep(1)
    np[0] = (0, 255, 0) # Color green
    np.write()
    sleep(1)
