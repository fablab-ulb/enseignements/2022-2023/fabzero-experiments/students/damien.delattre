import numpy as np
import matplotlib.pyplot as plt

from scipy.optimize import curve_fit



def gaussian(x, mean, amplitude, standard_deviation):
    return amplitude * np.exp( - (x - mean)**2 / (2*standard_deviation ** 2))



def plot(data):

    fig, ax = plt.subplots()
    bin_heights, bin_borders, _ = plt.hist(data, bins='auto', label='Mesures')
    bin_centers = bin_borders[:-1] + np.diff(bin_borders)/2
    popt, _ = curve_fit(gaussian, bin_centers, bin_heights, p0=[20.4, 100., 0.05])
    print(popt)
    y_fit = gaussian(bin_centers, *popt)
    #R_squared
    ss_res = np.sum((bin_heights - y_fit) ** 2)
    ss_tot = np.sum((bin_heights - popt[0]) ** 2)
    r2 = 1 - (ss_res / ss_tot)
    print("R2 = ", r2)
    textstr = '\n'.join((
    r'$\mu=%.3f$' % (popt[0], ),
    r'$\sigma=%.3f$' % (popt[2], ),
    r'$R^2=%.3f$' % r2 ))
    #Plot
    ax.set_xlabel("Température ")
    ax.set_ylabel("Nombre de mesures")
    props = dict(boxstyle='square', facecolor='white', alpha=0.25)
    ax.text(0.025, 0.80, textstr, transform=ax.transAxes, fontsize=11,
        verticalalignment='top', bbox=props)
    plt.plot(bin_centers, y_fit, label='Ajustement Gaussien')
    plt.legend()
    plt.show()


if __name__ == '__main__':

    data = np.loadtxt('/Users/damiendelattre/Desktop/Data.csv', delimiter=";")
    print(data)
    plot(data)







































# import numpy as np
# from scipy.optimize import curve_fit
# import matplotlib.pyplot as mpl

# # Let's create a function to model and create data
# def func(x, a, x0, sigma):
# 	return a*np.exp(-(x-x0)**2/(2*sigma**2))

# # Generating clean data
# x = np.linspace(0, 10, 100)
# y = func(x, 1, 5, 2)

# # Adding noise to the data
# yn = y + 0.2 * np.random.normal(size=len(x))

# # Plot out the current state of the data and model
# fig = mpl.figure()
# ax = fig.add_subplot(111)
# ax.plot(x, y, c='k', label='Function')
# ax.scatter(x, yn)

# # Executing curve_fit on noisy data
# popt, pcov = curve_fit(func, x, yn)

# #popt returns the best fit values for parameters of the given model (func)
# print (popt)

# ym = func(x, popt[0], popt[1], popt[2])
# ax.plot(x, ym, c='r', label='Best fit')
# ax.legend()
# fig.savefig('model_fit.png')
