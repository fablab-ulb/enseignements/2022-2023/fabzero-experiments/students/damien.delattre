#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
    #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif
#include "DHT20.h"

#define PIN        23
#define NUMPIXELS 1

Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

DHT20 DHT;

int8_t count = 0;

void setup() {

    // These lines are specifically to support the Adafruit Trinket 5V 16 MHz.
    // Any other board, you can remove this part (but no harm leaving it):
    #if defined(__AVR_ATtiny85__) && (F_CPU == 16000000)
    clock_prescale_set(clock_div_1);
    #endif
    // END of Trinket-specific code.

    pixels.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)

    // DHT.begin();    //  ESP32 default pins 21 22
    Wire.setSDA(8);
    Wire.setSCL(9);
    Wire.begin();


    Serial.begin(115200);
    Serial.println(__FILE__);
    Serial.print("DHT20 LIBRARY VERSION: ");
    Serial.println(DHT20_LIB_VERSION);
    Serial.println();

    delay(1000);
}


// void loop()
// {
//   if (millis() - DHT.lastRead() >= 5000)
//   {
//     // READ DATA
//     uint32_t start = micros();
//     int status = DHT.read();
//     uint32_t stop = micros();

//     if ((count % 10) == 0)
//     {
//       count = 0;
//       Serial.println();
//       Serial.println("Type\tHumidity (%)\tTemp (°C)\tTime (µs)\tStatus");
//     }
//     count++;

//     double_t temp = DHT.getTemperature();

//     if (temp < 2) {
//         pixels.setPixelColor(0, pixels.Color(0, 0, 150));
//         pixels.show();
//     }
//     else if (temp > 8){
//         pixels.setPixelColor(0, pixels.Color(150, 0, 0));
//         pixels.show();
//     }
//     else {
//         pixels.setPixelColor(0, pixels.Color(0, 150, 0));
//         pixels.show();
//     }

//     Serial.print("DHT20 \t");
//     // DISPLAY DATA, sensor has only one decimal.
//     Serial.print(DHT.getHumidity(), 1);
//     Serial.print("\t\t");
//     Serial.print(DHT.getTemperature(), 1);
//     Serial.print("\t\t");
//     Serial.print(stop - start);
//     Serial.print("\t\t");
//     switch (status)
//     {
//       case DHT20_OK:
//         Serial.print("OK");
//         break;
//       case DHT20_ERROR_CHECKSUM:
//         Serial.print("Checksum error");
//         break;
//       case DHT20_ERROR_CONNECT:
//         Serial.print("Connect error");
//         break;
//       case DHT20_MISSING_BYTES:
//         Serial.print("Missing bytes");
//         break;
//       case DHT20_ERROR_BYTES_ALL_ZERO:
//         Serial.print("All bytes read zero");
//         break;
//       case DHT20_ERROR_READ_TIMEOUT:
//         Serial.print("Read time out");
//         break;
//       case DHT20_ERROR_LASTREAD:
//         Serial.print("Error read too fast");
//         break;
//       default:
//         Serial.print("Unknown error");
//         break;
//     }
//     Serial.print("\n");
//   }
// }




void loop() {

    pixels.clear();
    Serial.println("Type\tTemp (°C)\tTime (µs)\tStatus");

    while (count < 10) {

        if (millis() - DHT.lastRead() >= 5000) {    // Break of 5 sec at least between two measurements
            // READ DATA
            uint32_t start = micros();
            int status = DHT.read();
            uint32_t stop = micros();

            double_t temp = DHT.getTemperature();

            if (temp < 2) {
                pixels.setPixelColor(0, pixels.Color(0, 0, 150));
                pixels.show();
            }
            else if (temp > 8){
                pixels.setPixelColor(0, pixels.Color(150, 0, 0));
                pixels.show();
            }
            else if (temp>2 && temp<8) {
                pixels.setPixelColor(0, pixels.Color(0, 150, 0));
                pixels.show();
            }
            else {
                pixels.clear();
            }

            Serial.print("DHT20 \t");
            // DISPLAY DATA, sensor has only one decimal.
            // Serial.print(DHT.getHumidity(), 1);
            // Serial.print("\t\t");
            Serial.print(temp, 3);
            Serial.print("\t\t");
            Serial.print(stop - start);
            Serial.print("\t\t");
            switch (status)
            {
            case DHT20_OK:
                Serial.print("OK");
                break;
            case DHT20_ERROR_CHECKSUM:
                Serial.print("Checksum error");
                break;
            case DHT20_ERROR_CONNECT:
                Serial.print("Connect error");
                break;
            case DHT20_MISSING_BYTES:
                Serial.print("Missing bytes");
                break;
            case DHT20_ERROR_BYTES_ALL_ZERO:
                Serial.print("All bytes read zero");
                break;
            case DHT20_ERROR_READ_TIMEOUT:
                Serial.print("Read time out");
                break;
            case DHT20_ERROR_LASTREAD:
                Serial.print("Error read too fast");
                break;
            default:
                Serial.print("Unknown error");
                break;
            }
            Serial.print("\n");

            count++;
        }
    }
    count = 0;
}