/*
    FILE   : 3D_model.scad
    
    AUTHOR : Damien Delattre <damien.delattre@ulb.be>
    
    DATE   : 2023-02-25
    
    LICENSE : Creative Commons Attribution-NonCommercial-ShareAlike 4.0 Unported [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).

    ADAPTED FROM : https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/jules.gerard/fabzero-modules/module02/
//*/

$fn=100;
side=7; // unité paramétrique
r_trous = 2; // rayons trous
r_sphere = 2; // rayons sphere

module barre_p(){ // 

    //barre
  rotate([0,0,90])
  translate([0,0,side*0.5])
    cube([side*10,side*0.8,side],center=true);


   //embout -2 
  rotate([0,0,90])
  translate([10*(side-1)/2,0,side])
    cylinder(side,(r_trous)-0.2,(r_sphere)-0.2,center=true);

    //embout -1 
  rotate([0,0,90])
  translate([10*(side-1)/4,0,side])
      cylinder(side,(r_trous)-0.1,(r_sphere)-0.1,center=true);

    //embout 0 
  rotate([0,0,90])
  translate([0,0,side])
    cylinder(side,(r_trous),(r_sphere),center=true);

    //embout +1
  rotate([0,0,90])
  translate([-10*(side-1)/4,0,side])
      cylinder(side,(r_trous)+0.1,(r_sphere)+0.1,center=true);

  //embout +2
  rotate([0,0,90])
  translate([-10*(side-1)/2,0,side])
    cylinder(side,(r_trous)+0.2,(r_sphere)+0.2,center=true);
}

barre_p();