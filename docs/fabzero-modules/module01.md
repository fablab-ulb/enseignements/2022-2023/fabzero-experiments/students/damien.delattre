# 1. Gestion de projet et documentation

 Le but de ce module est de préparer notre espace de travail, de commencer à documenter notre apprentissage ainsi que de s'habituer aux outils que nous allons utiliser tout au long de notre projet.

## 1.1 Ordinateur et éditeur de texte

L'ordinateur que j'utiliserai tout au long du cours et du développement du projet est un MacBook pro avec un processeur Intel. L'OS que j'utiliserai sera donc MacOS contrairement à la recommandation reçue d'utiliser Ubuntu.

Quant à l'éditeur de texte, je suis habitué à utiliser les éditeurs [atom](https://github.com/atom/atom) et [Visual Studio Code](https://code.visualstudio.com). Le premier étant très personnalisable et le second permettant d'exécuter du code directement. Tout au long du projet, j'utiliserai Visual Studio Code.


## 1.2 Git et GitLab

Je n'avais jamais utilisé *GitLab* mais j'étais déjà familier avec *Github*. Pour l'utilité que nous en aurons, ces deux outils sont très similaires.

### 1.2.1 Installer Git

[Git](https://git-scm.com) était déjà installé sur mon pc. Pour des gens n'ayant jamais travaillé avec cet outil, il existe plusieurs possibilités pour l'installer sur MacOS:
* Avec Homebrew:
  1. Télécharger [homebrew.](https://brew.sh)
  2. Dans un terminal, entrer la commande `brew install git`
* Avec Anaconda:
  1. Télécharger [anaconda.](https://www.anaconda.com/products/distribution)
  2. Dans un terminal, enter la commande `conda install -c anaconda git`
* Avec Xcode:
  L'application de développement d'Apple [Xcode](https://developer.apple.com/xcode/) installe automatiquement Git. Il suffit de télécharger Xcode dans l'AppStore, le lancer et le tour est joué.
* Télécharger un assistant d'installation:
  Il suffit de télécharger le fichier .dmg sur [cette page](https://sourceforge.net/projects/git-osx-installer/) et le lancer pour que l'assistant d'installation prenne toute la procédure en charge.

### 1.2.2 Commandes Git

Puisque je suis déjà habitué à l'utilisation de *Github*, je suis familier avec les commandes *Git*. Néanmoins, un petit rappel est toujours bénéfique ! 
![Alt text](images/git-cheat-sheet.jpg)
![Alt text](images/git-cheat-sheet2.jpg)

### 1.2.3 Clé SSH

J'ai pu remarquer que d'autres étudiants avaient des difficultés a générer leurs clés SSH et a entrer leur clé publique dans *GitLab* afin de créer une connexion sécurisée. Pour réaliser ces étapes, il suffit de

1. Ouvrir un terminal et d'enter la commande ```ssh-keygen```.
2. Enter dans le fichier caché contenant les clés ```cd .ssh```.
3. Enter la commande ```ls``` pour voir les éléments contenus dans le dossier `/.ssh`
4. Obtenir la clé publique (.pub) en entrant la commande ```cat 'nom_de_la_clé'.pub```
5. Copier la clé publique.
6. Coller la clé publique dans l'espace "SSH Keys" de *GitLab* et cliquer sur "ajouter la clé".

Dès lors, il est possible de cloner le dépôt *GitLab* en local (en entrant ```git clone 'url'``` dans le terminal) pour modifier le template mis à notre disposition.


## 1.3 Créer une page de présentation

Afin de nous familiariser avec le Markdown ou le HTML pour les plus courageux, nous devons créer une page de présentation où nous parlons essentiellement de nous. J'ai choisi d'écrire cette page en Markdown simplement parce que je trouve ça beaucoup plus simple que d'écrire du HTML.

### 1.3.1 Markdown

Le tableau suivant liste des exemples de [syntaxe Markdown](https://www.markdownguide.org/cheat-sheet).

| Syntaxe Markdown           | Rendu                                        |
| --------------------------| -------------------------------------------- |
| # Titre de niveau 1        | <h1>Titre de niveau 1</h1>                   |
| ## Titre de niveau 2       | <h2>Titre de niveau 2</h2>                   |
| ### Titre de niveau 3      | <h3>Titre de niveau 3</h3>                   |
| * Élément 1                | <ul><li>Élément 1</li></ul>                  |
| 1. Premier élément         | <ol><li>Premier élément</li></ol>            |
| `**Texte en gras**`          | <strong>Texte en gras</strong>               |
| `*Texte en italique*`        | <em>Texte en italique</em>                   |
| [Texte du lien](URL du lien) | <a href="URL du lien">Texte du lien</a>     |
| ![Texte alternatif](URL de l'image) | <img src="URL de l'image" alt="Texte alternatif"> |
| ` ​```code``` `         | Affiche un bloc de code                     |
| > Citation ici             | <blockquote>Citation ici</blockquote>       |
| Entête de colonne 1 \| Entête de colonne 2 | Tableau avec 2 colonnes          |
| Contenu de la colonne 1 \| Contenu de la colonne 2 | Lignes du tableau | 


### 1.3.2 Logiciels de traitement d'images

J'ai deux points à rapporter concernant le traitement des images et de la vidéo que j'ai inclus a la page. 
1. Pour réduire la résolution des images, j'utilise le logiciel [Gimp](https://www.gimp.org/downloads/). 
2. Pour réduire la résolution de la vidéo, j'utilise [ffmpeg](https://ffmpeg.org/download.html). L'installation de ce programme à été plus laborieuse. En effet, une fois décompressé, le fichier exécutable ne fonctionnait pas correctement. J'ai trouvé une solution à ce problème en installant ffmpeg via [Anaconda](https://anaconda.org/conda-forge/ffmpeg) qui est un gestionnaire de packages et d’environnements virtuels python.

<!-- # 1. Gestion de projet et documentation

This week I worked on defining my final project idea and started to getting used to the documentation process.

## title 1

"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

### subtitle 1

"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."


## title 2

"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.

## Table 1

| Qty |  Description    |  Price  |           Link           | Notes  |
|-----|-----------------|---------|--------------------------|--------|
| 1   | Material one    |  22.00 $| http://amazon.com/test   |    Order many    |
| 1   | Material two    |  22.00 $| http://amazon.com/test   |        |
| 1   | Material three  |  22.00 $| http://amazon.com/test   |        |
| 1   | Material five   |  22.00 $| http://amazon.com/test   |        |
| 1   | Material eight  |  22.00 $| http://amazon.com/test   |        |
| 1   | Material twelve |  22.00 $| http://amazon.com/test   |        |
| 1   | Material eleven |  22.00 $| http://amazon.com/test   |        |

## Useful links

- [Docsify](https://docsify.js.org/#/)
- [Markdown](https://en.wikipedia.org/wiki/Markdown)


## Code Example

Use the three backticks to separate code.

```
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
```


## Image gallery

![](../images/fablab-machine-logos.svg)

## Video

### From Vimeo

<iframe src="https://player.vimeo.com/video/10048961" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/10048961">Sound Waves</a> from <a href="https://vimeo.com/radarboy">George Gally (Radarboy)</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

### From Youtube

<iframe width="560" height="315" src="https://www.youtube.com/embed/Q3oItpVa9fs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## 3D Models

<div class="sketchfab-embed-wrapper"><iframe width="640" height="480" src="https://sketchfab.com/models/658c8f8a2f3042c3ad7bdedd83f1c915/embed" frameborder="0" allow="autoplay; fullscreen; vr" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
    <a href="https://sketchfab.com/models/658c8f8a2f3042c3ad7bdedd83f1c915?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Dita&#39;s Gown</a>
    by <a href="https://sketchfab.com/francisbitontistudio?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Francis Bitonti Studio</a>
    on <a href="https://sketchfab.com?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a>
</p>
</div> -->
