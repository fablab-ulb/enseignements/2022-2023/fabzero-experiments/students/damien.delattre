# Page d'acceuil

Hello World !

Cette page est une intro au cours de [2022-2023 ULB class "How To Make (almost) Any Experiments Using Digital Fabrication"](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website/). Elle contiendra essentiellement une présentation brève de mon profil.


## Qui suis-je ?

Je m'appelle Damien et je suis étudiant en master 2 en physique. J'ai fait mon mémoire en physique des hautes-énergies. Le titre de mon mémoire est *"Identification de processus de fusion de bosons vecteurs massifs au LHC"*. J'ai étudié les variables cinématiques des particules issues des collisions proton-proton de l'expérience Compact Muon Solenoïd (CMS) du Large Hadron Collider (LHC) au CERN à Genève et puis j'ai fait un réseau de neurone profond pour déterminer s'il était possible d'extraire une information supplémentaire permettant de savoir si un processus correspond bien à un certain mode de production rare du boson de Higgs. C'est une explication trop vulgarisée mais c'est difficile d'en dire pus sans avoir l'impression de donner un cours de physique des particules.
Outre la physique, je m'intéresse aussi à l'informatique. D'ailleurs j'imagine que je finirai probablement par trouver un job dans l'informatique. Ou bien qui sait, suivant l'avancée des technologies je pourrai peut-être combiner les deux avec l'informatique quantique...

J'aime bien jouer de la guitare électrique, j'ai une Gibson Les Paul standard qui sonne étonnament bien pour l'époque où je l'ai achetée. Pour l'accompagner, j'ai un ampli AMS fabriqué à la main par un électronicien en Belgique, une vraie tuerie ! En cliquant [ici](images/test.mp4), vous trouverez une vidéo de moi qui joue (parfois un peu faux mais je l'ai fait en une seule prise) un morceau de Led Zeppelin.

<!-- <video width="320" height="240" controls>
    <source src="/https://www.facebook.com/100009216791957/videos/2575706662746535" type="video/mp4">
</video> -->

![Guitare et ampli](images/IMG_1970.jpg "Guitare et ampli")

Cet hivers je suis parti skier en France, à la station des arcs. Si je pouvais, je partirais vivre là bas. Les paysages sont autrement plus beau que ceux que l'on a en Belgique. Depuis la chambre on avait une vue imprenable sur le versant italien du Mont-Blanc. 

![Alt text](images/PHOTO-2023-02-06-12-39-00.jpg)


<!-- ## Foreword

Hello!

This is an example student blog for the [2022-2023 ULB class "How To Make (almost) Any Experiments Using Digital Fabrication"](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website/).

Each student has a personal GitLab remote repository in the [GitLab class group](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students) in which a template of a blog is already stored. 

The GitLab software  is set to use [MkDocs](https://www.mkdocs.org/) to turn simple text files written in [Markdown](https://en.wikipedia.org/wiki/Markdown) format, into the site you are navigating.

#### Edit your blog

There are several ways to edit your blog

* by navigating your remote GitLab repository and [editing the files using the GitLab Web Editor](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#edit-a-file).
* by using [git](https://rogerdudler.github.io/git-guide/) and Command Line on your computer : cloning a local copy of your online project repository, editing the files locally and synchronizing back your local repository with the copy on the remote server.

#### Publishing your blog

Two times a week and each time you change a file, the site is rebuilt and all the changes are published in few minutes.

#### It saves history

No worries, you can't break anything, all the changes you make are saved under [Version Control](https://en.wikipedia.org/wiki/Version_control) using [GIT](https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control). This means that you have all the different versions of your page saved and available all the time in the Gitlab interface.

## About me

![](images/avatar-photo.jpg)

Hi! I am Emma Brewer. I am an art director & graphic designer based in New York City working on branding, visual identities, editorial design and web design.

Visit this website to see my work!

## My background

I was born in a nice city called..

## Previous work

I'm a paragraph. Edit the page on Gitlab to add your own text and edit me.  I’m a great place for you to tell a story and let your users know a little more about you.​

### Project A

This is an image from an external site:

![This is the image caption](https://images.unsplash.com/photo-1512436991641-6745cdb1723f?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=ad25f4eb5444edddb0c5fb252a7f1dce&auto=format&fit=crop&w=900&q=80)

While this is an image from the assets/images folder. Never use absolute paths (starting with /) when linking local images, always relative.

![This is another caption](images/sample-photo.jpg) -->
